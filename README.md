# Docker-asdf

Minimal Ubuntu-based docker image with [asdf](https://asdf-vm.com/) pre-installed.

## Commands

* Building the image: `docker build --pull -t registry.gitlab.com/aaronrenner/docker-asdf .`
* Pushing the image: `docker push registry.gitlab.com/aaronrenner/docker-asdf`
* Running interactively: `docker run -it registry.gitlab.com/aaronrenner/docker-asdf`
